# -*- coding: utf-8 -*-
"""
Created on Fri Jun 14 10:11:47 2019

@author: BHUSHAN
"""
#data analysis imports
import pandas as pd
import numpy as np
import random as rnd

#visualization imports
import matplotlib.pyplot as plt
import seaborn as sns

#machine learning imports
from sklearn.linear_model import LinearRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier

train_dataframe = pd.read_csv('train.csv')
test_dataframe = pd.read_csv('test.csv')

combine = ['train_dataframe','test_dataframe']

desciption_of_dataset = train_dataframe.describe()
#Analyising by pivotal features 
train_dataframe[['Pclass', 'Survived']].groupby(['Pclass'], as_index = False).mean().sort_values(['Survived'], ascending = False)
train_dataframe[['Sex','Survived']].groupby(['Sex'], as_index = False).mean().sort_values(['Survived'], ascending = False)
train_dataframe[['SibSp','Survived']].groupby(['SibSp'], as_index = False).mean().sort_values(['Survived'], ascending = False)
train_dataframe[['Parch', 'Survived']].groupby(['Parch'], as_index = False).mean().sort_values(['Survived'], ascending = False)

#Visualise Features
#Now we will analyse by visualising features like age 
g = sns.FacetGrid(train_dataframe, col= 'Survived')
g.map(plt.hist, 'Age', bins = 20)

#anlysing the correltion between Pclass and Survived and Visualising it by confirming our observation 
grid = sns.FacetGrid(train_dataframe, col='Survived', row='Pclass', size=2.2, aspect=1.6)
grid.map(plt.hist, 'Age', bins = 20, alpha = 0.5)
grid.add_legend()

#Adding 'Sex' for model training and checking the for embarkment too
grid = sns.FacetGrid(train_dataframe, row = 'Embarked', size=2.2, aspect=1.6)
grid.map(sns.pointplot, 'Pclass', 'Survived', 'Sex', palette = 'deep')
grid.add_legend()


#Adding 'Fare' feature for analysis 
grid = sns.FacetGrid(train_dataframe, row = 'Embarked', col= 'Survived', size=2.2, aspect=1.6)
grid.map(sns.barplot, 'Sex', 'Fare', alpha =0.5, ci = None)
grid.add_legend()

#droping ticket, cabin as they have no relation for predicting the survivors
train_dataframe = train_dataframe.drop(['Ticket', 'Cabin'], axis = 1)
test_dataframe = test_dataframe.drop(['Ticket','Cabin'], axis = 1)

#We need to drop Name and PassengerId too but before that we need to extract the useful information
for dataset in combine:
    dataset['Title'] = dataset.Name.str.extract(' ([A-Za-z]+)\.', expand = False)
    
pd.crosstab(train_dataframe['Title'], train_dataframe['Sex'])








